﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;

namespace Ja.MirrorTiles
{
	class TilesheetImg
	{
		readonly Image _img;

		internal Size Size { get { return _img.Size; } }
		internal Size TileSize { get; }
		internal PixelFormat PixelFormat { get { return _img.PixelFormat; } }

		internal TilesheetImg(Image img, Size tileSize)
		{
			_img = img;
			TileSize = tileSize;
		}
		internal static TilesheetImg Load(string inputPath, Size tileSize)
		{
			try
			{
				var img = Image.FromFile(inputPath);
				var tilesheet = new TilesheetImg(img, tileSize);
				return tilesheet;
			}
			catch (OutOfMemoryException)
			{
				throw new Exception("File has invalid file format or pixel format not supported. Supported file types are: BMP, GIF, JPEG, PNG and TIFF. For alpha transparancy use PNG with 32 bits per pixel.");
			}
			catch (FileNotFoundException)
			{
				throw new Exception("File not found.");
			}
			catch (ArgumentException)
			{
				throw new Exception("Filename is a Uri.");
			}
		}
		internal void Save(string outputPath)
		{
			try
			{
				_img.Save(outputPath);
			}
			catch (ArgumentNullException)
			{
				throw new Exception("Failed saving new sheet. Filename is null.");
			}
			catch (ExternalException)
			{
				throw new Exception("Failed saving new sheet. Wrong format or trying to save to original file.");
			}
		}
		internal Image Mirror(bool includeOriginals, int[] rowNumbersOfMultilineAnimations)
		{
			if (includeOriginals)
				return MirrorIncludeOriginals(rowNumbersOfMultilineAnimations);
			else
				return MirrorExcludeOriginals();
		}

		private Image MirrorExcludeOriginals()
		{
			var copies = GetTiles();
			var mirrored = TilesheetImg.FlipTilesHorizontally(copies);
			var rowsOfMirrored = BuildRows(mirrored);

			var builder = new TilesheetBuilder(TileSize, PixelFormat);
			foreach (var row in rowsOfMirrored)
				builder.AddRow(row);

			var newTilesheet = builder.Build();
			return newTilesheet;
		}
 		private Image MirrorIncludeOriginals(int[] rowNumbersOfMultilineAnimations)
		{
			var copies = GetTiles();
			var mirrored = TilesheetImg.FlipTilesHorizontally(copies);

			var rowsOfCopies = BuildRows(copies);
			var rowsOfMirrored = BuildRows(mirrored);

			var builder = new TilesheetBuilder(TileSize, PixelFormat);
			var mirrorQueue = new Queue<IList<Image>>();
			var noOfRows = Size.Height / TileSize.Height;
			for (var rowIndex = 0; rowIndex < noOfRows; ++rowIndex)
			{
				builder.AddRow(rowsOfCopies[rowIndex]);

				if (rowNumbersOfMultilineAnimations.Contains(rowIndex))
				{
					mirrorQueue.Enqueue(rowsOfMirrored[rowIndex]);
				}
				else
				{
					while (mirrorQueue.Count > 0)
						builder.AddRow(mirrorQueue.Dequeue());

					builder.AddRow(rowsOfMirrored[rowIndex]);
				}
			}

			var newTilesheet = builder.Build();
			return newTilesheet;
		}

		internal IList<Image> GetTiles()
		{
			var noOfCols = _img.Width / TileSize.Width;
			var noOfRows = _img.Height / TileSize.Height;

			using (var bmpSrc = new Bitmap(_img))
			{
				using (var fastBmpSrc = new FastBitmap(bmpSrc))
				{
					var tiles = new List<Image>();
					for (var rowIndex = 0; rowIndex < noOfRows; ++rowIndex)
					{
						for (var colIndex = 0; colIndex < noOfCols; ++colIndex)
						{
							var tile = GetTileAt(fastBmpSrc, colIndex, rowIndex);
							tiles.Add(tile);
						}
					}

					return tiles;
				}
			}
		}
		internal static List<Image> FlipTilesHorizontally(IList<Image> tiles)
		{
			var mirroredTiles =
				tiles
				.Select(t => FlipTileHorizontally(t))
				.ToList();

			return mirroredTiles;
		}

		private Image GetTileAt(FastBitmap src, int col, int row)
		{
			var pos = new Point(
				col * TileSize.Width,
				row * TileSize.Height);

			var tile = src.Copy(pos, TileSize);
			return tile;
		}
		private static Image FlipTileHorizontally(Image tile)
		{
			using (var bmp = new Bitmap(tile))
			{
				using (var fastBmp = new FastBitmap(bmp))
				{
					var flipped = fastBmp.FlipHorizontally();
					return flipped;
				}
			}
		}
		private IList<IList<Image>> BuildRows(IList<Image> tiles)
		{
			var noOfCols = Size.Width / TileSize.Width;

			var rows = new List<IList<Image>>();

			var row = new List<Image>();
			for (var i = 0; i < tiles.Count; ++i)
			{
				row.Add(tiles[i]);
				if (row.Count == noOfCols)
				{
					rows.Add(row);
					row = new List<Image>();
				}
			}

			return rows;
		}
	}
}
