﻿using System;
using System.Drawing;
using System.Drawing.Imaging;

namespace Ja.MirrorTiles
{
	internal class FastBitmap : IDisposable
	{
		readonly Bitmap _bitmap;
		readonly BitmapData _data;
		readonly int _bytesPerPixel;
		readonly int _widthInBytes;

		internal FastBitmap(Bitmap bitmap)
		{
			_bitmap = bitmap;
			_data = _bitmap.LockBits(new Rectangle(Point.Empty, _bitmap.Size), ImageLockMode.ReadWrite, _bitmap.PixelFormat);

			_bytesPerPixel = Bitmap.GetPixelFormatSize(_bitmap.PixelFormat) / 8;
			_widthInBytes = _bitmap.Width * _bytesPerPixel;
		}

		public void Dispose()
		{
			_bitmap.UnlockBits(_data);
		}

		internal Image FlipHorizontally()
		{
			var destBmp = new Bitmap(_bitmap.Width, _bitmap.Height, _bitmap.PixelFormat);

			using (var dest = new FastBitmap(destBmp))
			{
				unsafe
				{
					byte*
						ptrFirstSrcPxl = (byte*)_data.Scan0,
						ptrFirstDestPxl = (byte*)dest._data.Scan0;

					for (var y = 0; y < _bitmap.Height; ++y)
					{
						byte* currSrcLine = ptrFirstSrcPxl + (y * _data.Stride);
						byte* currDestLine = ptrFirstDestPxl + (y * dest._data.Stride);

						for (
							int srcX = 0, destX = _widthInBytes - _bytesPerPixel; 
							srcX < _widthInBytes; 
							srcX = srcX + _bytesPerPixel, destX = destX - _bytesPerPixel)
						{
							var red = currSrcLine[srcX];
							var green = currSrcLine[srcX + 1];
							var blue = currSrcLine[srcX + 2];
							var alpha = currSrcLine[srcX + 3];

							currDestLine[destX] = (byte)red;
							currDestLine[destX + 1] = (byte)green;
							currDestLine[destX + 2] = (byte)blue;
							currDestLine[destX + 3] = (byte)alpha;
						}
					}
				}

				return destBmp;
			}
		}
		internal Image Copy(Point pos, Size size)
		{
			var destBmp = new Bitmap(size.Width, size.Height, _bitmap.PixelFormat);

			using (var dest = new FastBitmap(destBmp))
			{
				unsafe
				{
					byte*
						ptrFirstSrcPxl = (byte*)_data.Scan0,
						ptrFirstDestPxl = (byte*)dest._data.Scan0;

					for (int srcY = pos.Y, destY = 0; destY < size.Height; ++srcY, ++destY)
					{
						byte* currSrcLine = ptrFirstSrcPxl + (srcY * _data.Stride);
						byte* currDestLine = ptrFirstDestPxl + (destY * dest._data.Stride);

						for (int srcX = pos.X * _bytesPerPixel, destX = 0; destX < _bytesPerPixel * size.Width; srcX = srcX + _bytesPerPixel, destX = destX + _bytesPerPixel)
						{
							int red = currSrcLine[srcX];
							int green = currSrcLine[srcX + 1];
							int blue = currSrcLine[srcX + 2];
							int alpha = currSrcLine[srcX + 3];

							currDestLine[destX] = (byte)red;
							currDestLine[destX + 1] = (byte)green;
							currDestLine[destX + 2] = (byte)blue;
							currDestLine[destX + 3] = (byte)alpha;
						}
					}
				}

				return destBmp;
			}
		}
	}
}
