﻿using System;

namespace Ja.MirrorTiles
{
	class Program
	{
		static void Main(string[] argArray)
		{
			try
			{
				Output.Msg($"Parse arguments.");
				var args = Args.Parse(argArray);

				Output.Msg($"Load tilesheet at: \"{args.InputPath}\".");
				var originalSheet = TilesheetImg.Load(args.InputPath, args.TileSize);

				Output.Msg($"Mirror tiles.");
				var mirroredSheet = originalSheet.Mirror(args.IncludeOriginals, args.RowsOfMultilineAnimations);

				Output.Msg($"Save tilesheet at: \"{args.OutputPath}\".");
				mirroredSheet.Save(args.OutputPath);
			}
			catch(Exception ex)
			{
				Output.Error(ex.Message);
				Output.BlankLine();
				Output.Usage();
			}

			Output.BlankLine();
		}
	}
}
