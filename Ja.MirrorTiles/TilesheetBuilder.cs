﻿using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;

namespace Ja.MirrorTiles
{
	internal class TilesheetBuilder
	{
		List<IList<Image>> _rows;
		Size _tileSize;
		PixelFormat _pixelFormat;
		int _noOfCols;

		internal TilesheetBuilder(Size tileSize, PixelFormat pixelFormat)
		{
			_tileSize = tileSize;
			_pixelFormat = pixelFormat;

			_rows = new List<IList<Image>>();
		}
		internal void AddRow(IList<Image> rowWithTiles)
		{
			_rows.Add(rowWithTiles);
			if (rowWithTiles.Count > _noOfCols)
				_noOfCols = rowWithTiles.Count;
		}
		internal Image Build()
		{
			var size = new Size(_noOfCols * _tileSize.Width, _rows.Count * _tileSize.Height);
			var sheet = new Bitmap(size.Width, size.Height, _pixelFormat);
			using (Graphics g = Graphics.FromImage(sheet))
			{
				for(int rowIndex = 0; rowIndex < _rows.Count; ++rowIndex)
					DrawRow(g, rowIndex);
			}

			return sheet;
		}

		private void DrawRow(Graphics g, int rowIndex)
		{
			var row = _rows[rowIndex];
			for(int colIndex = 0; colIndex < row.Count; ++colIndex)
			{
				var img = row[colIndex];
				var pos = new Point(
					colIndex * _tileSize.Width, 
					rowIndex * _tileSize.Height);

				g.DrawImage(img, pos);
			}
		}
	}
}