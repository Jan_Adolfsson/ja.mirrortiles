﻿using System;
using System.Drawing;
using System.IO;
using System.Linq;

namespace Ja.MirrorTiles
{
	internal class Args
	{
		internal string InputPath { get; }
		internal string OutputPath { get; }
		internal bool IncludeOriginals { get; }
		internal Size TileSize { get; }
		internal int[] RowsOfMultilineAnimations { get; }

		internal static Args Parse(string[] argArray)
		{
			if (argArray.Length < 3 ||
				argArray.Length == 5 ||
				argArray.Length > 6)
				throw new Exception("Invalid number of arguments, can be 3, 4 or 6.");

			var inputPath = argArray[0];
			var outputPath = argArray[1];
			var tileSize = ParseTileSize(argArray[2]);
			bool includeOriginals = ParseIncludeOriginals(argArray);
			var rowNumbers = ParseMultilineAnimations(argArray);

			var args = new Args(inputPath, outputPath, tileSize, includeOriginals, rowNumbers);
			return args;
		}

		private Args(string inputPath, string outputPath, Size tileSize, bool includeOriginals, int[] rowsOfMultilineAnimations)
		{
			InputPath = inputPath;
			OutputPath = outputPath;
			TileSize = tileSize;
			IncludeOriginals = includeOriginals;
			RowsOfMultilineAnimations = rowsOfMultilineAnimations;
		}
		private static Size ParseTileSize(string arg)
		{
			var sizeArgs = arg.Split(',');
			if (sizeArgs.Length < 2)
				throw new Exception("Invalid format of tile size, must be width,height e.g. 64,32.");

			if (!TryParsePositiveInt(sizeArgs[0], out int width))
				throw new Exception("Invalid format of tile width, must be an integer greater than zero.");

			if (!TryParsePositiveInt(sizeArgs[1], out int height))
				throw new Exception("Invalid format of tile height, must be an integer greater than zero.");

			var tileSize = new Size(width, height);
			return tileSize;
		}
		private static bool TryParsePositiveInt(string arg, out int value)
		{
			if (!int.TryParse(arg, out value))
				return false;

			if (value < 1)
				return false;

			return true;
		}
		private static bool ParseIncludeOriginals(string[] argArray)
		{
			if (argArray.Length < 4)
				return false;

			if (argArray[3] != "-i")
				throw new Exception("Fourth argument must be -i.");

			return true;
		}
		private static int[] ParseMultilineAnimations(string[] argArray)
		{
			if (argArray.Length != 6)
				return new int[0];

			if (argArray[4] != "-m")
				throw new Exception("Fifth argument must be -m.");

			var rowNumberStrings = argArray[5].Split(',');
			if (rowNumberStrings.Length == 0)
				throw new Exception("Invalid format of rows, must be list of comma separated integers, e.g. 4,6,10,14");

			var rowNumbers = ParseRowNumbers(rowNumberStrings);
			return rowNumbers;
		}
		private static int[] ParseRowNumbers(string[] rowNumberStrings)
		{
			var rowNumbers = rowNumberStrings.Select(ParseRowNumber).ToArray();
			return rowNumbers;
		}
		private static int ParseRowNumber(string arg)
		{
			if (!int.TryParse(arg, out int rowNumber))
				throw new Exception("Invalid format of row number, must be valid integer.");

			return rowNumber;
		}
	}
}