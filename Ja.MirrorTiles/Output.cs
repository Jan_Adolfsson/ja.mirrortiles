﻿using System;

namespace Ja.MirrorTiles
{
	class Output
	{
		internal static void BlankLine()
		{
			Console.WriteLine();
		}
		internal static void Msg(string msg)
		{
			Console.WriteLine(msg);
		}
		internal static void Error(string msg)
		{
			Console.WriteLine();
			var txtColor = Console.ForegroundColor;
			Console.ForegroundColor = ConsoleColor.Red;
			Console.WriteLine(msg);
			Console.ForegroundColor = txtColor;
		}
		internal static void Usage()
		{
			BlankLine();
			Msg("Mirror Tiles");
			Msg("============");
			Msg("Mirror tiles in tilesheet while maintaining same horizontal order.");
			Msg("");
			Msg("MILES source destination size [-i [-m rows]]");
			Msg("");
			Msg("  source		Specifies path to sheet with tiles to mirror.");
			Msg("  destination	Specifies path to new sheet with mirrored tiles.");
			Msg("  size			Size of tiles(width,height) found in source sheet, e.g. 64,32.");
			Msg("  -i			In the new sheet rows of mirrored tiles will be inserted between rows of original tiles.");
			Msg("  -m			Original sheet contains animations stretching over multiple rows.");
			Msg("  rows			Comma separated row numbers of animations continuing on next row, e.g. 5,6,10,14. Don't insert mirrored tiles after such a row");

			BlankLine();
		}
	}
}
