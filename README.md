# MILES  

MILES (Mirror Tiles) is a command line tool for mirroring all tiles in a tilesheet while maintaining their horizontal order. If you create a tilesheet with animations for a character facing right, then you can run this tool to either create a tilesheet containing the character facing both left and right, or a tilesheet containing the character facing left.  


### REMARKS  
A. Tech  
It's developed in C# using Visual Studio 2017 and for .NET Framework 4.6.2 but can easily be modified to run on earlier versions.  

The image processing is based on unsafe code so if you get in trouble working with the code you might have to open the Project Properties->Build and check Allow unsafe code.  

The image processing is based on numerous examples found on the net, just search for "fast image processing C#".  

B. Limitations  
The tool is only tested for 32 bpp argb PNGs.  
 
All tiles in sheet must be of the same size.  


### GET STARTED  
Clone repo:  
```
git clone https://Jan_Adolfsson@bitbucket.org/Jan_Adolfsson/Ja.MirrorTiles.git
```

Build:  
```
cd .\Ja.MirrorTiles\Ja.MirrorTiles\
dotnet build
```

Navigate to .exe:  
```
cd .\bin\debug
```


### USAGE  
miles.exe source destination size [-i [-m rows]]  

_source_  
Path to original tilesheet.  

_destination_  
Path to new tilesheet.  

_size_  
Tile size in pixels, e.g. 64,64  

_-i_  
Include original tiles in new tilesheet.  

_-m rows_  
Original tilesheet contains multiline animations. _Rows_ is a comma separated list of row numbers pointing at rows with animations continuing on next row.  


#### Create tilesheet with mirrored tiles only  
```
miles.exe c:\tiles\right_facing_hero.png c:\tiles\left_facing_hero.png 64,64
```


#### Create tilesheet containing both original and mirrored tiles  
A. No multiline animations.  
```
miles.exe c:\tiles\right_facing_hero.png c:\tiles\hero.png 64,64 -i
```

B. Multiline animations.  
Original tilesheet contains two multiline animations, one that begins on row 1 and continues on row 2 and ends on row 3, and one that begins on row 5 and continues to row 6. This is represented by -m 1,2,5, which basically means don't insert row of mirrored tiles after original lines 1, 2 and 5.  
```
miles.exe c:\tiles\right_facing_hero.png c:\tiles\hero.png 64,64 -i -m 1,2,5
```